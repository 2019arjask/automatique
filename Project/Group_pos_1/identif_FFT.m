function identif_FFT(Fmini, Fmaxi);

% definition du syst�me th�orique
% cd Datas;
% delete *;
% cd ..

T0=30;           % signal genere sur 30 secondes
pas=1e-3;        % pas d'echantillonnage
A=0.02; % amplitude de l'entree
Te=pas;
Nt=T0/pas;
t=[0:2*Nt-1]*pas;

i0=[Fmini*T0:round((Fmaxi-Fmini)*T0/20):Fmaxi*T0];  % i0 doit etre entier 

% i0=round(logSPACE(log10(Fmini*T0), log10(Fmaxi*T0), 10));

i0=i0(:);
f0=i0/T0;
Nf0 = length(f0);
T=repmat(t(:)',Nf0,1);
F0=repmat(f0,1,2*Nt);

s=sum(A*cos(2*pi*T.*F0)*2);


% 
% 
% % generation du signal d'entree
% entree.time = [0:Te:1-Te]';
% t2=[0:Te:1-Te]';
% entree.time=[entree.time;1+t'];
% entree.signals.values = zeros(1/Te,1);
% % generation d'une sinus auxiliaire pour les pb d'initiallisation
% 
% faux=Fmini;
% 
% 
% taux=[1-1/4/faux:Te:1-Te];
% 
% Sinaux=2*A*length(i0)*cos(2*pi*taux*faux);
% 
% entree.signals.values=[entree.signals.values(1:length(t2)-length(taux));Sinaux';s'];

entree.time = t;
entree.signals.values = s';

assignin('base', 'Te', pas);
assignin('base', 'T0', T0);
assignin('base', 'entree', entree);

% debut de simulation



