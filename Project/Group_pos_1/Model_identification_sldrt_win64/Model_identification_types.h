/*
 * Model_identification_types.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "Model_identification".
 *
 * Model version              : 1.40
 * Simulink Coder version : 9.0 (R2018b) 24-May-2018
 * C source code generated on : Thu Sep 26 09:31:35 2019
 *
 * Target selection: sldrt.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: Intel->x86-64 (Windows64)
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_Model_identification_types_h_
#define RTW_HEADER_Model_identification_types_h_
#include "rtwtypes.h"
#include "multiword_types.h"
#include "zero_crossing_types.h"
#ifndef DEFINED_TYPEDEF_FOR_struct_1NwfA1zW1fuBzUHxF8sFrE_
#define DEFINED_TYPEDEF_FOR_struct_1NwfA1zW1fuBzUHxF8sFrE_

typedef struct {
  real_T values[60000];
} struct_1NwfA1zW1fuBzUHxF8sFrE;

#endif

#ifndef DEFINED_TYPEDEF_FOR_struct_Io2RjVyGJC2KBZBPzxBbiG_
#define DEFINED_TYPEDEF_FOR_struct_Io2RjVyGJC2KBZBPzxBbiG_

typedef struct {
  real_T time[60000];
  struct_1NwfA1zW1fuBzUHxF8sFrE signals;
} struct_Io2RjVyGJC2KBZBPzxBbiG;

#endif

/* Parameters (default storage) */
typedef struct P_Model_identification_T_ P_Model_identification_T;

/* Forward declaration for rtModel */
typedef struct tag_RTM_Model_identification_T RT_MODEL_Model_identification_T;

#endif                                 /* RTW_HEADER_Model_identification_types_h_ */
