clear all
close all

Kx = 186;
taux = 0.016;

VxU = @(x,K,tau) K ./ (x.*(1+x.*tau).*(1+x.*0.001));

s = tf('s');
H = Kx/(s*(1 + taux*s)*(1+0.001*s));
H1 = Kx/(s*(1 + taux*s));

bode(H)
hold on
bode(H1)
grid on
