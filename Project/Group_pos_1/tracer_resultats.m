function tracer_resultat( entree, sortie);


T0=30;           % signal genere sur 30 secondes
pas=1e-3;
Te=pas;
Nt=T0/pas;
t=[0:2*Nt-1]*pas;
Fmaxi=evalin('base', 'Fmax');
Fmini=evalin('base', 'Freqmin');



Nfft=Nt;
i0=[Fmini*T0:round((Fmaxi-Fmini)*T0/20):Fmaxi*T0] ; % i0 doit etre entier 
% i0=round(logSPACE(log10(Fmini*T0), log10(Fmaxi*T0), 10));

A = 0.02;

f0=i0/T0;


y=sortie.signals.values;

s= entree.signals.values;

% calcul de la FFT.
fft_y=fft(y,Nfft)/Nt;
h=fft_y(i0+1)/A;
[phase_estimee,gain_estime] = cart2pol(real(h),imag(h));
phase_estimee=(angle(h));
gain_estime=abs(h);
phase_estimee = 180/pi*unwrap(phase_estimee) ;
PhCarte = 2*pi*f0*pas*180/pi;

phase_estimee = phase_estimee+PhCarte';

if min(phase_estimee)>0
    phase_estimee=phase_estimee-180;
end

assignin('base', 'hmesure', h);
assignin('base', 'f0', f0);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% on sait que c'est la position 



figure
semilogx(2*pi*f0,20*log10(gain_estime),'b*',2*pi*f0,20*log10(gain_estime),'b'); grid;
title('Réponse fréquentielle expérimentale : A1');
ylabel('Module [dB]');
xlabel('Pulsation [rad/s]')

axis([10 500 min(20*log10(gain_estime))-10 max(20*log10(gain_estime))+10]);

figure;
semilogx(2*pi*f0,phase_estimee,'b*',2*pi*f0,phase_estimee,'b'); grid;
title('Réponse fréquentielle expérimentale : A1')
xlabel('Pulsation [rad/s]');
ylabel('Phase [deg.]');
% axis([10 500 min(phase_estimee+Phcarte) max(phase_estimee+Phcarte)]);
%axis([10 500 -180 -90]);

if(min(phase_estimee)<-100) min1=-180;
else min1= -90;
end

if(max(phase_estimee)>-40) max1=0;
else max1= -90;
end

axis([10 500 min1 max1]);
