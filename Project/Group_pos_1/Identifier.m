function varargout = Identifier(varargin)

% global entree;
% global Frmin;
% global Frmax;


% IDENTIFIER M-file for Identifier.fig
%      IDENTIFIER, by itself, creates a new IDENTIFIER or raises the existing
%      singleton*.
%
%      H = IDENTIFIER returns the handle to a new IDENTIFIER or the handle to
%      the existing singleton*.
%
%      IDENTIFIER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in IDENTIFIER.M with the given input arguments.
%
%      IDENTIFIER('Property','Value',...) creates a new IDENTIFIER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Identifier_OpeningFunction gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Identifier_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Identifier

% Last Modified by GUIDE v2.5 23-Nov-2005 14:24:37

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Identifier_OpeningFcn, ...
                   'gui_OutputFcn',  @Identifier_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin & isstr(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Identifier is made visible.
function Identifier_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Identifier (see VARARGIN)

% Choose default command line output for Identifier
handles.output = hObject;
global voyant;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Identifier wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Identifier_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in LancerIdentif.
function LancerIdentif_Callback(hObject, eventdata, handles)
% hObject    handle to LancerIdentif (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

  
  set(handles.LancerIdentif,'Enable','off');
    
    set(handles.affiner,'Enable','off');
    set(handles.text8,'Enable','off');
    set(handles.TypePos,'Enable','off');
    set(handles.TypeVit,'Enable','off');
    set(handles.text11,'Enable','off');
    set(handles.text12,'Enable','off');


global model;
global voyant;
global Frmin;
global Frmax;

if (strcmp(get(handles.TracerBodeExp, 'Enable'),'off'))
    Frmin=0;
    Frmax=0;
end



% acquisition de la plage de fr�quence
 model='Model_identification';
 load_system(model);
etat= get_param(model, 'SimulationStatus');
if(strcmp(etat, 'stopped')~=0)
	
	Fmax=str2double(get(handles.Femax,'String'));
	if (isempty(Fmax)==1) Fmax=30;
	end
	assignin('base','Fmax',Fmax);
	
	Freqmin=str2double(get(handles.Femin,'String'));
	if (isempty(Freqmin)==1) Fraqmin=1;
	end
	assignin('base','Freqmin',Freqmin);
	
    if (Fmax~=Frmax)&(Freqmin~=Frmax)
        Frmin=Freqmin;
        Frmax=Fmax;

        identif_FFT(Freqmin, Fmax);



        load_system(model);
        disp('###################################################');
        disp('#################### Compilation ##################')
        disp('###################################################');
		
		rtwbuild ('Model_identification');
        % make_rtw;
        set(handles.LancerIdentif,'Enable','off');
    end
        
    cd Datas;
    delete *.mat;
    cd ..;
    
    disp('###################################################');
    disp('#################### D�but Simulation #############')
    disp('###################################################');
    set_param(model, 'SimulationMode', 'external');
    set_param(model, 'SimulationCommand', 'connect');
    pause(0.5)
    set_param(model, 'SimulationCommand', 'Start');
  set(handles.TracerBodeExp,'Enable','on');
else
    message_erreur;
end


% --- Executes on button press in TracerBodeExp.
function TracerBodeExp_Callback(hObject, eventdata, handles)
% hObject    handle to TracerBodeExp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% entree=evalin('base', 'Entree');
% sortie=evalin('base', 'Sortie');

set(handles.LancerIdentif,'Enable','on');

model='Model_identification';

etat= get_param(model, 'SimulationStatus');
if(strcmp(etat, 'stopped')~=0)
[sorties,entrees]=recuperation_donnees;
tracer_resultats(entrees, sorties);

    set(handles.affiner,'Enable','on');
    set(handles.text8,'Enable','on');
    set(handles.TypePos,'Enable','on');
    set(handles.TypeVit,'Enable','on');
    set(handles.text11,'Enable','on');
    set(handles.text12,'Enable','on');

else
    warning('Sequance de mesures en cours. Le trace Bode est foncionel seulement apres le message <<Model Model_identification unloaded>>');%message_erreur;
    set(handles.LancerIdentif,'Enable','off');
end


% --- Executes during object creation, after setting all properties.
function Femin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Femin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function Femin_Callback(hObject, eventdata, handles)
% hObject    handle to Femin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Femin as text
%        str2double(get(hObject,'String')) returns contents of Femin as a double
if get(handles.Femax,'String') 
    set(handles.LancerIdentif,'Enable','on');
end

% --- Executes during object creation, after setting all properties.
function Femax_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Femax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc
    set(hObject,'BackgroundColor','white');
else
    set(hObject,'BackgroundColor',get(0,'defaultUicontrolBackgroundColor'));
end



function Femax_Callback(hObject, eventdata, handles)
% hObject    handle to Femax (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Femax as text
%        str2double(get(hObject,'String')) returns contents of Femax as a double
if get(handles.Femin,'String') 
    set(handles.LancerIdentif,'Enable','on');
end


% --- Executes on button press in affiner.
function affiner_Callback(hObject, eventdata, handles)
% hObject    handle to affiner (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

model='Model_identification';

etat= get_param(model, 'SimulationStatus');
if(strcmp(etat, 'stopped')~=0)
    
    f0 = evalin('base', 'f0');
    h=evalin('base', 'hmesure');
    Type= get(handles.Femax,'String');
    Type2 = (get(handles.TypeVit,'Value'));
    Type1 =(get(handles.TypePos,'Value'));
    
    % 	if(Type1==Type2)
    %         message_acqui;
    % 	else
    if (Type1==1)
        Type='pos';
    else
        Type='vit';
    end    
    Trace_Bode(h, 2*pi*f0, Type);

    % 	end
    
else
    message_erreur;
end

% --- Executes on button press in choixPos.
function choixPos_Callback(hObject, eventdata, handles)
% hObject    handle to choixPos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of choixPos



% --- Executes on button press in TypePos.

% --- Executes on button press in TypePos.
function TypePos_Callback(hObject, eventdata, handles)
% hObject    handle to TypePos (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(hObject,'Value',1);
set(handles.TypeVit,'Value',0);


% Hint: get(hObject,'Value') returns toggle state of TypePos


% --- Executes on button press in TypeVit.
function TypeVit_Callback(hObject, eventdata, handles)
% hObject    handle to TypeVit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(hObject,'Value',1);
set(handles.TypePos,'Value',0);
% Hint: get(hObject,'Value') returns toggle state of TypeVit


