function Trace_Bode(hmesure, wmesure, Type)


hmesure = hmesure(:);

% initialisation

K=1;
tau=1;



% DEBUT ...
% a cet endroit il faut un wmesure et les valeurs complexes hmesure

magmesure=20*log10(abs(hmesure));
phasemesure=unwrap(angle(hmesure))*180/pi;
pas=1e-3;

PhCarte = wmesure'*pas*180/pi;

phasemesure = phasemesure+PhCarte;


if min(phasemesure)>0
    phasemesure=phasemesure-180;
end


wcandidat=logspace(log10(min(wmesure)/2),log10(max(wmesure)*4),1000);


if(strcmp(Type, 'pos')==1)
danscadre=1;
while danscadre
    sysref=tf(K,[tau 1 0]);
    hcandidat =freqresp(sysref,wcandidat);
    hcandidat = hcandidat(:);
    magcandidat=20*log10(abs(hcandidat));
    phasecandidat=unwrap(angle(hcandidat))*180/pi;
    
    acandidat =freqresp(tf(K,[1 0]),wcandidat);
    acandidat = acandidat(:);
    magacandidat=20*log10(abs(acandidat));
    phaseacandidat=unwrap(angle(acandidat))*180/pi;
    
    aacandidat =freqresp(tf(K,[tau 0 0]),wcandidat);
    aacandidat = aacandidat(:);
    magaacandidat=20*log10(abs(aacandidat));
    phaseaacandidat=unwrap(angle(aacandidat))*180/pi;
    
    titrefigure='identification interactive';
    addfigure=findobj('type','figure','name',titrefigure);
    if isempty(addfigure)
        figure
        set(gcf,'numbertitle','off')
        set(gcf,'name',titrefigure)
    else
        figure(addfigure)
    end
    add(1)=subplot(211);
    semilogx(wmesure,magmesure,'*r',wcandidat,magacandidat,'b',...
        wcandidat,magaacandidat,'m',wcandidat,magcandidat,'k',...
        K,0,'*b',K,0,'ob');
    axis([min(wmesure)*0.8 max(wmesure)*2 -20 30]);
    title(sprintf('K = %0.3g     tau = %0.2g s',K,tau));
    ylabel('Module [dB]');

    grid on
    add(2)=subplot(212);
    semilogx(wmesure,phasemesure,'*r',wcandidat,phasecandidat,'k',...
        1/tau,-135,'om',1/tau,-135,'*m')
    grid on
   axis([min(wmesure)*0.8 max(wmesure)*2 -200 -80]);
xlabel('Pulsation [rad/s]')
ylabel('Phase [deg.]');

    [x,y]=ginput(1);
    addax=gca;
    a=axis;
    danscadre=(x>a(1))&(x<a(2))&(y>a(3))&(y<a(4));
    if danscadre
        if addax==add(1);
%             disp('choix d''une valeur de K')
            K=x;
        elseif addax==add(2);
%             disp('choix d''une valeur de tau')
            tau=1/x;
        else
%             disp('erreur figure')
        end
    end
end

else
    danscadre=1;
while danscadre
    sysref=tf(K,[tau 1]);
    hcandidat =freqresp(sysref,wcandidat);
    hcandidat = hcandidat(:);
    magcandidat=20*log10(abs(hcandidat));
    phasecandidat=unwrap(angle(hcandidat))*180/pi;
    
    acandidat =freqresp(tf(K,[1]),wcandidat);
    acandidat = acandidat(:);
    magacandidat=20*log10(abs(acandidat));
    phaseacandidat=unwrap(angle(acandidat))*180/pi;
    
    aacandidat =freqresp(tf(K,[tau 0]),wcandidat);
    aacandidat = aacandidat(:);
    magaacandidat=20*log10(abs(aacandidat));
    phaseaacandidat=unwrap(angle(aacandidat))*180/pi;
    
    titrefigure='identification interactive';
    addfigure=findobj('type','figure','name',titrefigure);
    if isempty(addfigure)
        figure
        set(gcf,'numbertitle','off')
        set(gcf,'name',titrefigure)
    else
        figure(addfigure)
    end
    add(1)=subplot(211);
    semilogx(wmesure,magmesure,'*r',wcandidat,magacandidat,'b',...
        wcandidat,magaacandidat,'m',wcandidat,magcandidat,'k',...
        min(wcandidat),20*log10(K),'*b',min(wcandidat),20*log10(K),'ob')
           
    axis([min(wmesure)*0.8 max(wmesure)*2 -20 20]);
ylabel('Module [dB]');

    title(sprintf('K = %0.3g     tau = %0.2g s',K,tau))
    grid on
    add(2)=subplot(212);
    semilogx(wmesure,phasemesure,'*r',wcandidat,phasecandidat,'k',...
        1/tau,-45,'om',1/tau,-45,'*m')
    grid on
       axis([min(wmesure)*0.8 max(wmesure)*2  -100 10]);
xlabel('Pulsation [rad/s]')
ylabel('Phase [deg.]');

    [x,y]=ginput(1);
    addax=gca;
    a=axis;
    danscadre=(x>a(1))&(x<a(2))&(y>a(3))&(y<a(4));
    if danscadre
        if addax==add(1);
%             disp('choix d''une valeur de K')
            K=10^(y/20);
        elseif addax==add(2);
%             disp('choix d''une valeur de tau')
            tau=1/x;
        else
%             disp('erreur figure')
        end
    end
end
end

assignin('base', 'K', 'K');
assignin('base', 'tau', 'tau');

disp('###################################################');
disp('##################### Résultats ###################');
disp('###################################################');
disp(['K = ', num2str(K, '%0.3g') ])
disp(['tau =  ', num2str(tau, '%0.2g')])
