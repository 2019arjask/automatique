function F = Transfert_systeme(X, Xdata)

K=X(1);

n=1;
tau=X(2);


Num=K;
integ=[1];
for j=1:n
integ=conv(integ, [1 0]);
end
Den=conv(integ,[tau 1]);

sys=tf(Num,Den);

[mag,phase] = bode(sys,Xdata);

% [Re, Im]= pol2cart( unwrap(squeeze(phase))*pi/180,squeeze(mag));

F=[mag, phase];