close all
clear all

K = 0.0187;
tau = 0.018;

s = tf('s');
H = K/((1 + tau*s)*(1+0.001*s));
H1 = K/((1 + tau*s));
bode(H)
hold on
bode(H1)
grid on
legend("With electrical time constant","without el.t.const")