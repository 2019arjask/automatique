\documentclass{article}

\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{dot2texi}
\usepackage{tikz}
\usepackage{comment}
\usetikzlibrary{shapes,arrows}

\setcounter{section}{2}
\begin{document}

\section{Modeling - Identification}
\setoutputdir{latex.out/}
\begin{figure}
    \centering
    \begin{dot2tex}[dot,mathmode, autosize]
        digraph G{
            ranksep=0.1;
            nodesep=0.05;
            splines=ortho;
            {rank=sama SS}
            {rank=same ST J0 C1 C2 J1 J2 A M R EN}
            {rank=same PS}
            C1 [label="C_1(s)", shape=box, weight=100]
            C2 [label="C_2(s)", shape=box]
            J0 [label="", shape=circle]
            J1 [label="", shape=circle]
            J2 [label="", shape=circle]
            A  [label="A", shape=box]
            M  [label="motor", shape=box]
            R  [label="Gear", shape=box]
            PS [label="Position", shape=box]
            SS [label="Speed", shape=box]
            ST [label="", shape=none]
            EN [label="", shape=none] 
            ST-> J0 [label="e(t)"];
            J0 -> C1[ label=""];
            C1 -> J1[ label="v(t)"];
            J1 -> C2[label = ""];
            C2 -> J2[label = "u(t)"];
            J2 -> A;
            A->M;
            M->SS[label="\Omega(t)"];
            M->R[label="\theta_m(t)"];
            R->EN[label="x(t)"];
            R->PS;
            PS->J0[taillabel="V_x(t)"];
            SS->J1[taillabel="V_\Omega(t)"];
        }
    \end{dot2tex}
\caption{The whole system}
\label{fig:full}
\end{figure}

\subsection{Modeling based on a priori knowledge}

The open loop transfer functions $\frac{V_x(s)}{U(s)}$ and $\frac{V_\Omega(s)}{U(s)}$ can be obtained from figure \ref{fig:full} and are displayed in figure \ref{fig:ex31}.
The offsets are set to 0 in the following calculations

\begin{figure}[ht]
    \centering
    \begin{dot2tex}[dot,mathmode, autosize]
        digraph G{
            ranksep=0.1;
            nodesep=0.05;
            splines=ortho;
            {rank=same ST J2 A M R PS EN}
            J2 [label="", shape=circle]
            A  [label="A", shape=box]
            M  [label="motor", shape=box]
            R  [label="Gear", shape=box]
            PS [label="Position", shape=box]
            ST [label="", shape=none]
            EN [label="", shape=none] 
            ST -> J2[label = "u(t)"];
            J2 -> A;
            A->M[label="u_m(t)"];
            M->R[label="\theta_m(t)"];
            R->PS;
            PS->EN[taillabel="V_x(t)"];
        }
    \end{dot2tex}
    \caption{Open loops for exercise 3.1}
    \label{fig:ex31}
\end{figure}

The output signal from the position is 
\begin{equation}
    V_x(t) = p(t)*r(t)*m(t)*A(t)*u(t)
\end{equation}
Where $p(t)$ is the function of the position sensor, $r(t)$ is the signal of the Reduction gear, $m(t)$ is the signal of the motor and $A(t)$ is the signal of the Amplifier.
Taking the Laplace transform gives us
\begin{equation}
    V_x(s) = P(s)R(s)M(s)A(s)U(s)\leftrightarrow \frac{V_x(s)}{U(s)} = P(s)R(s)M(s)A(s)
\end{equation}
The amplifier is taken to be a constant ($A(s) = A$).
In the lecture notes, the transfer function of the motor was derived
\begin{equation}
    M(s) = \frac{\omega_m(s)}{U_m(s)} = \frac{K_v}{s(1 + \tau s )(1 + \tau's)}
\end{equation}
where $K_v$ is the speed constant of the motor, $\tau$ is the mechanical time constant of the motor and $\tau'$ is the electrical time constant of the motor.
The Reduction gear is used to change the incident angular velocity by some constant $1/N$.
This value is simply the ratio of the number of teeths in each gear, and thus the transfer function
\begin{equation}
    \frac{\theta_s(s)}{\theta_m(s)} = \frac{1}{N}
\end{equation}

The angular velocity from the Reuduction gears is converted to linear displacement $x(t) = \rho \theta_s(t)$ with $\rho$ being the radius of the pulley.
Due to linearity of the Laplace transform, the transfer function is 
\begin{equation}
    \frac{X(s)}{\theta_s(s)} = \rho \Rightarrow R(s) = \frac{X(s)}{\theta_s(s)}\frac{\theta_s(s)}{\theta_m(s)} = \frac{\rho}{N}
\end{equation}
In the position sensor, there's a linear relation between the voltage and distance $V_x(t) = \beta x(t)$.
Thus
\begin{equation}
    P(s) = \frac{V_x(s)}{X(s)} = \beta
\end{equation}

Thus the transfer equation 
\begin{equation}
    \frac{V_x(s)}{U(s)} = \frac{\beta \rho}{N}\frac{A K_v}{s(1 + \tau s)(1 + \tau's)} \approx \frac{K_a}{s(1 + \tau s)}
    \label{eq:Vx}
\end{equation}
\setoutputdir{latex.out/}

\begin{figure}[ht]
    \centering
    
    \begin{dot2tex}[dot,mathmode, autosize]
        digraph G{
            splines=ortho;
            {rank=same SS ST J2 A M EN}
            J2 [label="", shape=circle]
            A  [label="A", shape=box]
            M  [label="motor", shape=box]
            SS [label="Speed", shape=box]
            ST [label="", shape=none]
            EN [label="", shape=none] 
            ST -> J2[label = "u(t)"];
            J2 -> A;
            A->M[label= "u_m(t)"];
            M->SS[label="\Omega(t)"];
            SS->EN[taillabel="V_\Omega(t)"];
        }
    \end{dot2tex}
    \caption{The open loop of $V_\Omega$}
    \label{fig:ex31b}
\end{figure}
Similarly, the transfer function
\begin{equation}
    V_\Omega(t) = s(t)*m_\Omega(t)*A(t)*u(t) \Rightarrow \frac{V_\Omega(s)}{U(s)} = S(s)M_\Omega(s)A(s)
\end{equation}
Where $M_\Omega$ is the transfer function of motor to angular velocity and $S(s)$ is the transfer function of the speed sensor.
In Appendix 1, the transfer function of motor was given as
\begin{equation}
    M_\Omega(s) = \frac{\Omega(s)}{U_m(s)} = \frac{K_v}{(1 + \tau s)(1 + \tau's)}
\end{equation}
The sensor is considered a gain $g$, meaning that the relation between angular frequency $\Omega_t(t)$ and speed is linear.
Thus
\begin{equation}
    S(s) = \frac{V_\Omega(s)}{\Omega(s)} = g
\end{equation}
Collecting all of the values together, we obtain
\begin{equation}
    \frac{V_\Omega(s)}{U(s)} = \frac{Ag K_v}{(1 + \tau s)(1 + \tau's)} \approx \frac{K_b}{1 + \tau s}
    \label{eq:Vo}
\end{equation}


\subsection{Identification}

By fitting into the measurements, as seen in figure \ref{fig:speed} and \ref{fig:pos}, it was found that $K_1 \approx 187$, $K_2 \approx 0.187$ and $\tau \approx 0.017\pm 0.001$.
By substituting into equations \ref{eq:Vx} and \ref{eq:Vo} we can evaluate
\begin{align*}
    K_a = \frac{\beta \rho}{N}A K_v &\leftrightarrow K_v = \frac{N K_1}{\beta \rho A}  = \frac{187}{0.6\cdot 10}\approx 31\\
    K_b = Ag K_v &\leftrightarrow g = \frac{K_2}{AK_v} \approx \frac{0.187}{10\cdot 31} \approx 6.0\times 10^{-4}
\end{align*}
Here the Amplitude $A$ was set to 10.



The second order system can be approximated as a first order system by dropping the second time constant.
This approximation is valid because $\tau' << \tau$ and thus $1/\tau'$ is outside the frequency range in question.
This is demonstrated in figures \ref{fig:bodes1} and \ref{fig:bodes2}.

\section{Control design: frequency-based approach}
\subsection{Block diagram}
The requested block diagram can be found in figure \ref{fig:full_t}.
\setoutputdir{latex.out/}
\begin{figure}[ht!]
    \centering
    \begin{dot2tex}[dot,mathmode, autosize]
        digraph G{
            ranksep=0.1;
            nodesep=0.05;
            splines=ortho;
            {rank=sama SS}
            {rank=same ST J0 C1 C2 J1 J2 A M R EN}
            {rank=same PS}
            C1 [label="C_1(s)", shape=box, weight=100]
            C2 [label="C_2(s)", shape=box]
            J0 [label="", shape=circle]
            J1 [label="", shape=circle]
            J2 [label="", shape=circle]
            A  [label="A", shape=box]
            M[label="\frac{K_v}{(1 + \tau s)}\Big/\frac{K_v}{s(1 + \tau s)}", shape=box]
            R  [label="\frac{\rho}{N}", shape=box]
            PS [label="\beta", shape=box]
            SS [label="Speed", shape=box]
            ST [label="", shape=none]
            EN [label="", shape=none] 
            ST-> J0 [label="e(t)"];
            J0 -> C1[ label=""];
            C1 -> J1[ label="v(t)"];
            J1 -> C2[label = ""];
            C2 -> J2[label = "u(t)"];
            J2 -> A;
            A->M;
            M->SS[label="\Omega(t)"];
            M->R[label="\theta_m(t)"];
            R->EN[label="x(t)"];
            R->PS;
            PS->J0[taillabel="V_x(t)"];
            SS->J1[taillabel="V_\Omega(t)"];
        }
    \end{dot2tex}
\caption{The whole system in terms of transfer functions}
\label{fig:full_t}
\end{figure}

\subsection{Speed loop}
It has been established that the transfer function $\frac{\Omega(s)}{U(s)} = \frac{K_v A}{1 + s\tau}$ and it has time constant $\tau$.
The closed loop consists of open loop
\begin{equation}
    H(s) = G(s)\beta(s) = \underbrace{K_2(s)A\frac{K_v}{1 + s\tau}}_{=G(s)} g
    \label{eq:Hs}
\end{equation}
The closed loop has transfer function
\begin{equation}
    F(s) = \frac{G(s)}{1 + G(s)\beta(s)} = \frac{\frac{K_2 A K_v}{1 + s\tau}}{1 + g\frac{K_2AK_v}{1 + s\tau}} = \frac{K_2AK_v}{(1 + g\underbrace{K_2AK_v}_{=K}) + s\tau}
\end{equation}
Manipulatingthe equation in to similar form as equation \ref{eq:Hs}
\begin{align*}
    F(s) = \frac{\frac{K}{1 + gK}}{1 + \frac{\tau}{1 + g K}s} = \frac{K_2'}{1 + \tau_2s}
\end{align*}
Thus we see that $F(s)$ is a first order system that has a time constant $\tau/(1 + g K)$.
Now we can evaluate the condition for $K_2$
\begin{align*}
    \frac{\tau}{2} &> \frac{\tau}{1 + g K } = \frac{\tau}{1 + gK_2 K_v A}\\
    2 &< 1 + gK_2K_vA \leftrightarrow 1 < gK_2K_vA\\
    \Rightarrow K_2 &> \frac{1}{gK_vA}
\end{align*}

Substituting the values evaluated earlier
\begin{equation*}
    K_2 \gtrapprox \frac{6.0\times 10^4}{31\cdot10}\approx 5.4 \Rightarrow F(s) \approx \frac{835}{1 + \tau_2 s}
\end{equation*}



\subsection{Position loop}

Let us assume that with the controller the system is a general second order system.
First, the first maxima should be achieved at $t_m = 30$ms.
This means that the system has a crossover frequency at 
\begin{equation*}
    \omega_c \approx \frac{3}{30\text{ms}} = 100\text{rad/s}
\end{equation*}
To limit the overshoot to 20\%, at this frequency the phase margin should be $\approx 47 \rightarrow 50$ degrees.
As can be seen from the measurements in figure \ref{fig:pos}, the phase margin at 100 rad/s is only approximately 30 degrees.
This means that the phase margin needs to be improved, which can only be done with a derivative controller.
However, we also need an integrative controller for avoiding the steady state error from input and amplifier offset.
Thus, let us propose a PID controller for $C_1(s)$.

\begin{equation}
    C_1(s) = K\frac{(1 + T_is + T_iT_ds^2}{T_is} \approx K\frac{1 + T_1s)(1 + T_2s)}{T_i s}
\end{equation}
if $T_i >> T_d$.
Now the position loop transfer function reads
\begin{equation}
    H(s) = K\frac{(1 + T_1s)(1 + T_2s)}{T_i s} \frac{K_2'}{1 + \tau_2s} \frac{D}{s}
\end{equation}
The calculation can be simplified slightly by setting $T_2 = \tau_2$, allowing us to ignore one of the terms.
By estimating the phase asymptotically, at 100 rad/s we have contributions from atleast two integrators, making the phase -180 degrees.
To counteract this, we need to set $1/T_1 < \omega_c$ and $T_1 > 0$, so the phase can be improved.
The electrical constant $\tau'$ which is introducing a second pole at $10^3$, even though ignored, it is still affecting the phase to differ from the desired.
If it is included, the transfer function corresponds to a generalized first order system with double integrators.
The generalized order system has maximal phase improvement at $\omega = (\tau' T_1)^{-1/2}$, so setting $\omega = \omega_c$ and solving for $T_1$ we obtain
\begin{equation*}
    T_1 = \frac{1}{\tau' \omega_c^2} = \frac{1}{10^{-3}\cdot 10^4}\text{s} = 0.1\text{s}
\end{equation*}
From here, we get the PID parameters $T_i = T_1 + T_2 \approx 0.1 + 0.017/2 \approx 0.1$ and $T_d = T_1T_2/T_i \approx T_2 = 0.017/2 \approx 0.009$.
Whether this phase improvement is enough or not, is to be decided with matlab.
Now only thing left to do, is to set the gain so that the crossover is at $\omega_c$.
Mathematically, dB($H(\omega_c)$) = 0 $\rightarrow H(\omega_c) = 1$.
\begin{align*}
H(\omega_c) &= K\frac{1 + T_1 \omega_c}{T_i \omega_c^2} K'D = 1 \Rightarrow K = \frac{T_i \omega_c^2}{(1 + T_1 \omega_c) K'D}\\
\Rightarrow K&\approx \frac{0.1 \cdot 100^2}{(1 + 0.1\cdot 100)\cdot 835\cdot 0.6} \approx 0.18
\end{align*}

The model is plotted in figure \ref{fig:open_pos} and it has phase margin $\Delta \varphi \approx 83^\circ$.

\subsection{Validation through simulation}
The step response of the closed loop system is depicted in figure \ref{fig:step}.

\begin{figure}[!ht]
    \centering
    \includegraphics[width=\textwidth]{Images/step.png}
    \caption{The step response of the position system. The maxima is reached around the right time and the overshoot is within the limits. There is no steady state error in the system.}
    \label{fig:step}
\end{figure}

\begin{comment}
%\footnote{After doing the calculation, I realized that it would have been enough to consider wether the integrator is before or after the error.
%    So here's some sort of mathematical proof anyways I guess..}
    Let us the propose that $C_1(s)$ would be a PI controller, so that $C_1(s) = K_1(1 + 1/T_i s)$.
    
    Thus the closed position loop transfer function
\begin{align*}
    \frac{X(s)}{E(s)} = \frac{K_1\left (1 +\frac{1}{T_i s }\right)F(s) \frac{1}{s}}{1 + K_1\left (1 +\frac{1}{T_i s }\right)F(s) \frac{1}{s} \beta} = \frac{F(s)K_1}{s^2 + K_1 F(s) \beta}
\end{align*}
where $F(s)$ is the closed loop transfer function of the speed loop.
Since the time constant for $F(s)$ is greater than the time constant in the outer system, $F(s)$ can be approximated as a simple gain $F(s) \approx 1/g(0) = 1/g = \alpha$ (ch. 5.3. in the book).
Thus
The system is now a second order system with 

By demanding that the step response peak time is at 30 ms, we can evaluate the crossover frequency $\omega_c$.
\begin{equation*}
    \omega_c \approx \frac{3}{30ms} = 100 \text{Hz}
\end{equation*}
It means that at this frequency the system needs to have a phase margin of atleast ~47 $\rightarrow$ 50 degrees to limit the overshoot to 20\%.



Splitting the path into 
\begin{equation}
    G_1(s) = C_1(s)K_2, G_2(s) = \frac{K_0}{s(s + \tau s)}, \beta(s) = \beta
\end{equation}
The steady state error is found when $t\rightarrow \infty \leftrightarrow s \rightarrow 0$.
The error is zero if and only if (Equation 3.47 in the book)
\begin{equation}
    \lim_{s\rightarrow 0}G_2(s)\beta(s) = 0 \text{ or } G_1(s) \propto_{s\rightarrow 0} \frac{K_1}{s^\alpha}, \alpha \geq 1
\end{equation}
Since $\lim_{s\rightarrow 0} G_2 = \infty$, $G_2$ must be at least type 1 system.
Dismissing steady state error requires introducing an integrator into the system.
Because $C_2$ doesn't contain an integrator, it implies that $C_1 = \frac{K_1}{s}$.
Thus the open loop transfer function of the position loop

\end{comment}




\clearpage









\section*{Homework}
\subsection*{The digitalized system}
\setoutputdir{latex.out/}
\begin{figure}
    \centering
    \begin{dot2tex}[dot,mathmode, autosize]
        digraph G{
            ranksep=0.1;
            nodesep=0.05;
            splines=ortho;
            {rank=min SS AD1}
            {rank=same OFF}
            {rank=same ST J0 C1 C2 J1 J2 A M R EN DAC}
            {rank=same PS AD2}
            C1 [label="C_1(z)", shape=box, weight=100]
            C2 [label="C_2(z)", shape=box]
            J0 [label="", shape=circle]
            J1 [label="", shape=circle]
            J2 [label="", shape=circle]
            A  [label="A", shape=box]
            M  [label="motor", shape=box]
            R  [label="Gear", shape=box]
            PS [label="Position", shape=box]
            SS [label="Speed", shape=box]
            ST [label="", shape=none]
            EN [label="", shape=none] 
            OFF[label="", shape=none]
            AD1[label="ADC", shape=box]
            AD2[label="ADC", shape=box]
            DAC[label="DAC", shape=box]
            ST-> J0 [label="e(t)"];
            J0 -> C1[ label=""];
            C1 -> J1[ label="v(t)"];
            J1 -> C2[label = ""];
            C2 -> J2[label = "u(t)"];
            J2 -> DAC;
            DAC->A;
            A->M;
            M->SS[label="\Omega(t)"];
            M->R[label="\theta_m(t)"];
            R->EN[label="x(t)"];
            R->PS;
            AD2->PS[taillabel="V_x(t)",dir=back];
            AD2->J0;
            AD1->SS[taillabel="V_\Omega(t)",dir=back];
            AD1->J1;
            OFF->J2[label="offset"];
        }
    \end{dot2tex}
\caption{The system with digital control}
\end{figure}






\clearpage
\appendix
\section{Proof why an integrator negates steady state error}
The task of the offset is to negate any effects originating from the DC motor.
It is assumed to be constant for the duration of the measurement.
This means, that the sensor should stay stationary when there's no input.
In other words, $e(t) = 0$ should mean $\lim_{t\rightarrow \infty}x(t) = 0$ for some constant $b(t) = B$.
With the final value theorem, this problem can be reformulated as
\begin{equation*}
    \lim_{s\rightarrow 0}sX(s) = \lim_{s\rightarrow 0}T(s)B = 0
\end{equation*}
where $T(s)$ is the transfer function $X(s)/B(s)$. 
It should be noted that the extra $s$ disappears from the limit due to the assumption that $b(t)$ is constant $\mathcal{L}\{B\} = B/s$.
$T(s)$ can be written by reorganizing the block diagram \ref{fig:full_t} starting from the open loop
\begin{equation}
    H_b(s) = C_2(s)\beta C_1(s) \frac{D}{s}F'(s)
\end{equation}
where $F'(s)$ is the transfer function of the inner closed loop
\begin{equation}
    F'(s) = \frac{A\frac{K_v}{1+\tau s}}{1 - g C_2 A\frac{K_v}{1+\tau s}} = \frac{A K_v}{1 + \tau s - gC_2AK_v} = \frac{K'}{1 + \tau's}
\end{equation}
with this we obtain the closed loop transfer function $T(s)$ as
\begin{align*}
    T(s) = \frac{\frac{D}{s}F'(s)}{1 + \frac{D}{s}F'(s)C_1(s)\beta} = \frac{DK'}{s(1 + \tau's) +DK'\beta C_1(s)}
\end{align*}
For this to be zero as $s\rightarrow 0$, the simplest form $C_1(s)$ can take is $C_1(s) = K_1/s$.
Now
\begin{align*}
    T(s) = \frac{DK'}{s(1 + \tau's) +DK'\beta \frac{K_1}{s}} = \frac{DK's}{s^2(1 + \tau's) +DK'\beta K_1} \rightarrow \lim_{s\rightarrow 0}T(s) = 0
\end{align*}
Thus, to negate the effect of $B$, controller $C_1(s)$ needst to have a term of the form $C_1'(s) = K_1'/s$.
\section{Plots}
\begin{figure}[ht!]
    \centering
    \includegraphics[width=\textwidth]{Images/initial_fit_speed.png}
    \caption{The speed measurement and parametric fit. The lowest measurement was made at $\omega \approx 19$ and highest at $\omega \approx 180$.}
    \label{fig:speed}
\end{figure}
\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{Images/initial_fit_position.png}
    \caption{The position measurement with the corresponding parametric fit}
    \label{fig:pos}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{Images/Bodes.png}
    \caption{The difference introduced by removing time constant in the $V_x(s)/U(s)$ loop. The frequency range of themeasurements is marked with points. As can be seen, the curves are equal within the frequency range with only slight deviation in phases.}
    \label{fig:bodes1}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{Images/Bodes2.png}
    \caption{The difference introduced by removing time constant in the $V_\Omega(s)/U(s)$ loop. Again, the curves are almost identical within the frequency range. There's slightly more deviation in the phase compared to figure \ref{fig:bodes1}}
    \label{fig:bodes2}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{Images/H1.png}
    \caption{The open loop transfer function for position loop.}
    \label{fig:open_pos}
\end{figure}

\end{document}
















