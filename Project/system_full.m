close all

s = tf('s');

K = 0.18;
Kp = 835;
t1 = 0.018;
t2 = 0.009;
T1 = 0.1;
Ti = 0.1;
D = 0.6;

H2 = Kp/(1 + t2*s);

%bode(H2)

H1 = K*(1 + T1*s)*D*Kp/((Ti*s^2));
H = H1/(1 + 0.001*s);
[Gm, Pm, Wcg, Wcp] = margin(H1)
bode(H1,{0.1,120})
%hold on
%bode(H,{0.1,120})
%grid on
G = H1/1.97;
B = 1.97;
F = feedback(G,B);
%bode(F
%step(F)
grid on