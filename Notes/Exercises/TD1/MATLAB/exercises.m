clear all;

s = tf('s');
sys = 10^-2*(1-2*s)/(s*(1 + 20*s)); 
%bode(sys);

%nyquist(sys);

dK = 50;

sys2 = dK*sys;

%bode(sys2);
%nyquist(sys2);
[Gm1,Pm1,Wcg1,Wcp1] = margin(sys);
[Gm2,Pm2,Wcg2,Wcp2] = margin(sys2);

H2 = 0.2/(1 + 2*0.4*s/1.5 + s^2/(1.5*1.5));
%subplot(2,1,1)
%bode(H2);

%subplot(2,1,2)
%step(H2);

%SYS = feedback(H2)

Gs = 1000*sys;
Bs = 10^-2;

Fs = feedback(Gs,Bs);
bode(Fs)

step(Fs)