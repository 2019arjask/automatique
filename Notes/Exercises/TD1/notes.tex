\documentclass{article}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{hyperref}
\usepackage{kvantti}
\usepackage{graphics}
\usepackage{fullpage}
\usepackage[utf8]{inputenc}


\usepackage{graphviz}
\usepackage{kvantti}

\usepackage{dot2texi}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}

\graphicspath{{Images/}}

\renewcommand{\thesubsection}{\thesection.\alph{subsection})}


\begin{document}

The system works on the following parameters:
\begin{itemize}
    \item $N_e$ : Water level in the generator
    \item $Q_v$ : Steam demand, considered as a disturbance
    \item $Q_e$ : Water flow rate, considered as the control input
\end{itemize}

In the frequency domain, the water level is given by
\begin{equation}
    N_e(s) = \frac{1}{s}\sulut{H_1(s) Q_e(s) - H_2(s)Q_v(s)}
\end{equation}

The actuator for the $Q_e$ is a valve whose opening depends on required voltage $u$. It is modelled by a transfer function $H_v(s)$.

The water level $N_e$ is measured by a sensor and it provides the information as a voltage $V_{N_e}$.
It has a gain $K_c$.

The water level is regulated with a proportional control with gain $K>0$.
The setpoint $V_{N_c}$ is set to the desired level $N_c$.
This gives the control equations $U(s) = K\epsilon(s)$, where $\epsilon(s) = V_{N_c} (s) - V_{N_e}(s)$.

\section{Continuous time}
\subsection{}
\setoutputdir{latex.out/}

\begin{figure}[ht]
    \centering
    \begin{dot2tex}[dot,mathmode, autosize]
        digraph G{
            ranksep=0.1;
            nodesep=0.05;
            splines=ortho;
            {rank=same PT H2}
            {rank=same ST EN H1 J0 J1 IN K0 HV}
            subgraph cluster_0 {
                label=""
                H1 [label="H_1(s)", shape=box];
                H2 [label="H_2(s)", shape=box];
                HV [label="H_v(s)", shape=box];
                J1 [label="", shape=circle];
                IN [label="\frac{1}{s}", shape=box]
            }
            ST [label="", shape=none]
            PT [label="", shape=none]
            EN [label="", shape=none] 
            KC [label="K_c", shape=box]
            J0 [label="", shape=circle]
            K0 [label="K", shape=box]
            ST -> J0 [label="V_e(s)", headlabel="+"]
            H1 -> J1 [taillabel="+"];
            H2 -> J1 [headlabel="-"];
            PT -> H2 [label="Q_v(s)"];
            J1 -> IN;
            IN -> EN [label="N_e(s)"];
            IN -> KC;
            KC -> J0 [taillabel="V_{N_e}", headlabel="-"];
            J0 -> K0 [label="\epsilon(s)"];
            K0 -> HV [label="U(s)"];
            HV -> H1 [label="Q_e(s)"];
        }

    \end{dot2tex}
    \caption{The block diagram of the system}
\label{fig:full}
\end{figure}

\subsection{}
The open loop expression from $V_{N_e}$ to $\epsilon$.
\begin{equation*}
    \frac{V_{N_e}(s)}{\epsilon(s)} = K_c \frac{1}{s}H_1(s)H_v(s)K
\end{equation*}

\subsection{}
Having a term containging $s$ in the denominator decreases bends the curve downwards at the pole and having such term in the nominator bends it upwards.
The curve starts at $k ~-1$ since it has one integrator $\frac{1}{s}$.
The slope doubles at $\frac{1}{\tau_1}$, so there must be a pole in the denominator.
Since the phase decreases, it means that there's a term of type $1/(1 + \tau_1 s)$ meaning that $s = -1/\tau_1$
Thus reading from the graph, we obtain $-1/\tau_1 \approx 0.05$.
At $\frac{1}{\tau_2}$ the slope increases back to $~-1$ so there's a zero in the nominator.
However, since the phase still decreases, there's a term of the type $s - \tau_2s$.
This equates to $s = \tau_2 \approx 0.5$.
Since the system has one integrator, $K_c$ is found to be same magnitude as the cross-over frequency $K_c \rightarrow 10^{-2}$.

From these we find
\begin{equation*}
    \tau_1 \approx - 20, \tau_2 \approx 2
\end{equation*}
\begin{figure}[ht]
    \centering
    \includegraphics[width=\textwidth]{bode_fits.png}
    \caption{Approximate fits on the bode diagrams}
    \label{fig:fit}
\end{figure}


\begin{table}[ht]
    \centering
    \begin{tabular}{r|c|c}
         & $\varphi \uparrow$ & $\varphi \downarrow$\\
        \hline
        $\frac{dG}{d\omega} \uparrow$ & $1 + \tau s$ & $1 - \tau s$\\
        \hline
        $\frac{dG}{d\omega} \downarrow$ & $\frac{1}{1 - \tau s}$ &$\frac{1}{1 + \tau s}$\\
    \end{tabular}
    \caption{Table for finding poles from Bode plots}
    \label{tb:bode}
\end{table}

\subsection{}
Please refer to figure \ref{fig:nyquist}.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.7\textwidth]{nyquist.png}
    \caption{The Nyquist plot approximated from values extracted from the Bode plot}
    \label{fig:nyquist}
\end{figure}
\subsection{}
From the Bode plot,$\Delta \varphi \approx 180^\circ - 100^\circ = 80^\circ$ and $x \approx -35 \text{dB} \approx 0.02 \rightarrow \Delta G \approx \frac{1}{0.02} = 50 = K_{\text{lim}}$.

\subsection{}
The system reaches its first maximum at $\omega_c t \approx 3 \rightarrow \omega_c = \frac{3}{t}$.
If $t = 50$(s), then $\omega_c \approx 0.06$.
This means that $|H(s)|$ should be 0dB at 0.06. 
Since adding Gain to the system simply moves the Bode diagram vertically and when $K = 1 \rightarrow |H(0.06)|\approx -20$, we need to set $K = 20$dB$=10$.

The margins for the new system are \footnote{$y$ dB = $20\log_{10}y$}
\begin{equation}
    \varphi_2 \approx 150^\circ \rightarrow \Delta\varphi_2 = 30^\circ, x \approx -10\text{dB} = 0.2 \rightarrow \Delta K = 5
\end{equation}

Since a delay is of the form $e^{-Tj\omega}$, where $T$ is the time delayed.
It is clear, that this is a complex number on the complex unit circle with angle $\varphi = -T\omega_c$.
Delay margin can be found from $\varphi = -180^\circ \Rightarrow T = \varphi/\omega_c \approx \pi/0.06$
\subsection{}
\subsection{}

We are studying the effect of a step disturbance $Q_v$ on the system, so $Q_e = 0$.
For this we are given the almost linear plot $V_{N_c}(t) \approx kt$.
Performing the Laplace transform yields the transfer function
\begin{equation}
    V_{N_c}(s) = \mathcal{L}\{kt\} = \frac{k}{s^2} = -K_c\frac{1}{s}H_2(s)Q_v = -\frac{K_c H_2}{s^2}
\end{equation}
Since $Q_v(t) = 1 \rightarrow Q_v(s) = 1/s$.
Thus
\begin{equation}
    K_cH_2(s) = -k \approx -\frac{1.0}{5.0} = -0.2
\end{equation}

\subsection{}
Since we are looking the relation between $V_{N_e}$ and $V_{N_c}$, we shall ignore $Q_e$.

\begin{align*}
    \frac{V_{N_e}(s)}{V_{N_c}(s)} &= \frac{G(s)}{1 + \beta(s)G(s)}\\
    G(s) &= KH_1(s)\frac{1}{s} \approx \frac{10\cdot (1 - 2s)}{(1 + 20s)}\\
    \beta(s) &= K_c = 10^{-2} \\
    \Rightarrow \frac{V_{N_e}(s)}{V_{N_c}(s)} &\approx \frac{10\frac{10\cdot (1 - 2s)}{(1 + 20s)}}{1 + 0.1\frac{10\cdot (1 - 2s)}{(1 + 20s)}} \approx \frac{10(1 - 2s)}{20s^2+0.8s+0.1}
\end{align*}


\end{document}














